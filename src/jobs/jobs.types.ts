export type JobSummary = {
  id: number;
  title: string;
  location: string;
  timestamp: number;
  division?: string;
  subdivision?: string;
  employmentType?: string;
};

export type JobQuestionType = {
  required: boolean;
  label: string;
  name: string;
  type: string;
};

export type JobDetails = JobSummary & {
  content: string;
  questions: JobQuestionType[];
};

export type JobFilters = {
  departments: string[];
  employmentTypes: string[];
  locations: string[];
};
