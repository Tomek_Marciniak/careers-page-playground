import { Injectable, NotFoundException } from '@nestjs/common';
import uniques from 'src/utils/array/uniques';
import { GreenhouseService } from '../greenhouse/greenhouse.service';
import { JobDetails, JobSummary, JobFilters } from './jobs.types';

@Injectable()
export class JobsService {
  constructor(private greenhouseService: GreenhouseService) {}

  async getJobsList(): Promise<JobSummary[]> {
    const jobs = await this.greenhouseService.getAvailableJobs();

    return jobs.map(
      ({
        id,
        title,
        location,
        division,
        subdivision,
        employmentType,
        timestamp,
      }: JobDetails) => ({
        id,
        title,
        location,
        division,
        subdivision,
        employmentType,
        timestamp,
      }),
    );
  }

  async getJobDetails(id: number): Promise<JobDetails> {
    const jobs = await this.greenhouseService.getAvailableJobs();
    const jobDetails = jobs.find((job) => job.id === id);

    if (!jobDetails) {
      throw new NotFoundException(`Error while fetching job, id: ${id}`);
    }
    return jobDetails;
  }

  async getFilters(): Promise<JobFilters> {
    const jobs = await this.greenhouseService.getAvailableJobs();
    const departments = [];
    const employmentTypes = [];
    const locations = [];

    jobs.forEach(({ division, subdivision, location, employmentType }) => {
      departments.push(division);
      departments.push(subdivision);
      employmentTypes.push(employmentType);
      locations.push(...location.split(';'));
    });

    const parseOutput = (data: string[]) =>
      uniques(data.map((s) => s && s.trim()).filter(Boolean)).sort();

    return {
      departments: parseOutput(departments),
      employmentTypes: parseOutput(employmentTypes),
      locations: parseOutput(locations),
    };
  }
}
