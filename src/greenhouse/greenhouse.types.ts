export interface HarvestJobPost {
  id: number;
  active: boolean;
  live: boolean;
  first_published_at: string;
  title: string;
  location: Location;
  internal: boolean;
  external: boolean;
  job_id: number;
  content: string;
  internal_content: null;
  updated_at: string;
  created_at: string;
  demographic_question_set_id: null;
  questions: HarvestJobPostQuestion[];
}

export interface HarvestJob {
  id: number;
  name: string;
  requisition_id: string;
  notes: null;
  confidential: boolean;
  is_template: boolean;
  copied_from_id: null;
  status: string;
  created_at: string;
  opened_at: string;
  closed_at: null;
  updated_at: string;
  departments: Department[];
  offices: Office[];
  hiring_team: HiringTeam;
  openings: Opening[];
  custom_fields: HarvestJobCustomFields;
  keyed_custom_fields: HarvestJobKeyedCustomFields;
}

export interface HarvestJobPostLocation {
  id: number;
  name: string;
  office_id: null;
  job_post_location_type: HarvestJobPostLocationType;
}

export interface HarvestJobPostLocationType {
  id: number;
  name: string;
}

export interface HarvestJobPostQuestion {
  required: boolean | null;
  private: boolean;
  label: string;
  name: string;
  type: string;
  values: any[];
  description: null;
}

export interface HarvestJobCustomFields {
  '*subdivision': string;
  division: string;
  group: null;
  team_name: null;
  technology: null;
  '*matrix_area': null;
  employment_type: string;
  level_of_position: null;
  reporting_manager: ValueClass;
  top_of_the_market_range: null;
  market_comp_information: null;
  'requisition_status_(recruiter_only)': string;
  'requisition_status_comments_(recruiter_only)': string;
  'exception_process_2022_(talent_acquisition_only)': string;
  gross_salary_range_minimum: GrossSalaryRangeMinimum;
}

export interface GrossSalaryRangeMinimum {
  value: number;
  unit: string;
}

export interface ValueClass {
  name: string;
  email: string;
  user_id: number;
}

export interface Department {
  id: number;
  name: string;
  parent_id: null;
  parent_department_external_id: null;
  child_ids: any[];
  child_department_external_ids: any[];
  external_id: string;
}

export interface HiringTeam {
  hiring_managers: Coordinator[];
  recruiters: Coordinator[];
  coordinators: Coordinator[];
  sourcers: any[];
}

export interface Coordinator {
  id: number;
  first_name: string;
  last_name: string;
  name: string;
  employee_id: null;
  responsible?: boolean;
}

export interface HarvestJobKeyedCustomFields {
  subdivision: Division;
  division: Division;
  group: null;
  team_name: null;
  technology: null;
  engineering_back_end_or_front_end: null;
  employment_type: Division;
  level_of_position: null;
  reporting_manager: KeyedCustomFieldsReportingManager;
  top_of_the_market_range: null;
  market_comp_information: null;
  search_status__recruiter_only_: Division;
  requisition_status_comments: Division;
  exception_process_2022: Division;
  salary_range__in_local_currency__numbers_only_: SalaryRangeInLocalCurrencyNumbersOnly;
}

export interface Division {
  name: string;
  type: string;
  value: string;
}

export interface KeyedCustomFieldsReportingManager {
  name: string;
  type: string;
  value: ValueClass;
}

export interface SalaryRangeInLocalCurrencyNumbersOnly {
  name: string;
  type: string;
  value: GrossSalaryRangeMinimum;
}

export interface Office {
  id: number;
  name: string;
  location: Location;
  primary_contact_user_id: null;
  parent_id: null;
  parent_office_external_id: null;
  child_ids: any[];
  child_office_external_ids: any[];
  external_id: string;
}

export interface Location {
  name: string;
}

export interface Opening {
  id: number;
  opening_id: string;
  status: string;
  opened_at: string;
  closed_at: null | string;
  application_id: number | null;
  close_reason: CloseReason | null;
  custom_fields: OpeningCustomFields;
  keyed_custom_fields: OpeningKeyedCustomFields;
}

export interface CloseReason {
  id: number;
  name: string;
}

export interface OpeningCustomFields {
  headcount_type: string;
  replacement_name: null;
  'business_justification_/_rationale': null | string;
  'why_can_no_one_else_on_the_team_carry_out_this_role_(e.g._skills)': null;
  'what_will_happen_to_the_business_if_this_position_is_not_filled_in_the_next_3_months?': null;
  '*team_name': null;
}

export interface OpeningKeyedCustomFields {
  headcount_type: Division;
  'replacement_name_opening_1626106350.296618': null;
  business_justification: Division | null;
  why_can_no_one_else_on_the_team_carry_out_this_role__e_g__skills_: null;
  what_will_happen_to_the_business_if_this_position_is_not_filled_in_the_next_3_months_: null;
  team_name: null;
}
