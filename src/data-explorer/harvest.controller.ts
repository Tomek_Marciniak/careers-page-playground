import { Controller, Get } from '@nestjs/common';
import { HarvestApiService } from '../greenhouse/harvest-api/harvest-api.service';

@Controller('explore/harvest')
export class HarvestController {
  constructor(private readonly harvestApiService: HarvestApiService) {}
  @Get('/jobs')
  getJobsList() {
    return this.harvestApiService.getJobs();
  }
  @Get('/jobPosts')
  getJobPosts() {
    return this.harvestApiService.getJobPosts();
  }
}
